@extends("layouts.master")

@section("title")
    Home
@endsection

@section("content")
    {{--<div class="page-content home-page-1">

        <!-- END OF SLIDER WRAPPER-->
        <div id="CarouselSlider" class="carousel carousel-dark slide" data-bs-ride="carousel" data-bs-interval="false">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#CarouselSlider" data-bs-slide-to="0" class="active"
                        aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#CarouselSlider" data-bs-slide-to="1"
                        aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#CarouselSlider" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
                <button type="button" data-bs-target="#CarouselSlider" data-bs-slide-to="3"
                        aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{asset('img/slider/compare-1900.png')}}" class="d-block w-100" alt="Carousel Two" >
                    <div class="carousel-caption text-white d-none d-md-block col-6">
                        <h3 class="fs-4 fw-bold">Compare Insurance</h3>
                        <p>Compare Insurance Quotes and get the best deal from over 50 Insurance Companies.</p>
                        <a href="compare-insurance" class="btn btn-primary mt-3">Start Now</a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{asset('img/slider/invest-1900.png')}}" class="d-block w-100" alt="Carousel One">
                    <div class="carousel-caption text-white d-none d-md-block col-6">
                        <h3 class="fs-4 fw-bold">Why We Invest</h3>
                        <p>We have brought together top insuranceExperts to
                            <br/>provide you the best free advise on insurance and finance matters.</p>
                        <a href="#" class="btn btn-warning mt-3">Learn More</a>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="{{asset('img/slider/questions-1900.png')}}" class="d-block w-100" alt="Carousel Three">
                    <div class="carousel-caption text-white d-none d-md-block col-6">
                        <h3 class="fs-5 fw-bold">Got an Insurance Question?</h3>
                        <p> Search our Database of our 1000 answered Insurance Questions,
                            <br/> or better still ask your question on our platform and
                            <br/> one of our export will sorely answer you within 24 hours
                            <br/></p>
                        <a href="#" class="btn btn-success mt-3">Search Now</a>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#CarouselSlider" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#CarouselSlider" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>




        <!-- TESTINMONIAL, STYLE 1-->
        <section class="testinmonials testinmonials-layout style-1">
            <div class="container">
                <div class="heading">
                    <h3 class="heading-section">What Our Customers Say</h3>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <!-- testinmonials item-->
                        <div class="testinmonial-item style-1 match-item">
                            <div class="content">
                                <p>
                                    Morbi auctor vel mauris facilisis lacinia. Aenean suscipit
                                    lorem leo, et hendrerit odio fermentum et. Donec ac dolor
                                    eros. Mauris arcu nunc, iaculis sit amet lacus iaculis,
                                    faucibus faucibus nunc. Vestibulum sit amet lacinia massa
                                </p>
                            </div>
                            <div class="personal">
                                <div class="avatar">
                                    <img alt="Cheryl Cruz" src="{{asset('img/avatar/avatar-1.jpg') }}"/>
                                </div>
                                <div class="info">
                                    <div class="name">
                                        <span>Cheryl Cruz</span>
                                    </div>
                                    <div class="title-job">
                                        <span>Car Owner</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <!-- testinmonials item-->
                        <div class="testinmonial-item style-1 match-item">
                            <div class="content">
                                <p>
                                    Vestibulum ultricies cursus feugiat. Vestibulum scelerisque
                                    posuere neque. Phasellus tortor lacus, tincidunt nec varius
                                    ut, tempor eget massa. Aenean non lorem ex. Phasellus
                                    dapibus eu justo dapibus commodo.
                                </p>
                            </div>
                            <div class="personal">
                                <div class="avatar">
                                    <img alt="Linda Campbell" src="{{asset('img/avatar/avatar-2.jpg') }}"/>
                                </div>
                                <div class="info">
                                    <div class="name">
                                        <span>Linda Campbell</span>
                                    </div>
                                    <div class="title-job">
                                        <span>Car Owner</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <!-- testinmonials item-->
                        <div class="testinmonial-item style-1 match-item">
                            <div class="content">
                                <p>
                                    Etiam et purus in dui dignissim dictum quis efficitur
                                    libero. Sed fringilla, augue vel sollicitudin bibendum,
                                    ligula elit rhoncus lorem, a egestas diam augue sed enim. In
                                    eget luctus ante. Mauris ut cursus nunc, vitae hendrerit
                                </p>
                            </div>
                            <div class="personal">
                                <div class="avatar">
                                    <img alt="John Walker" src="{{asset('img/avatar/avatar-3.jpg') }}"/>
                                </div>
                                <div class="info">
                                    <div class="name">
                                        <span>John Walker</span>
                                    </div>
                                    <div class="title-job">
                                        <span>Home Owner</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END TESTINMONIAL, STYLE 1-->


    </div>--}}
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="hero d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-flex flex-column justify-content-center">
                    <h1 data-aos="fade-up">Democratizing Health and Insurance</h1>
                    <h2 data-aos="fade-up" data-aos-delay="400">
                        Compare Insurance Quotes and get the best deal from over 50 Insurance Companies
                    </h2>
                    <div data-aos="fade-up" data-aos-delay="600">
                        <div class="text-center text-lg-start">
                            <a href="compare-insurance"
                               class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                                <span>Get a Quote</span>
                                <i class="bi bi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
                    <img src="{{asset('img/home.gif') }}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- End Hero -->

    <!-- ======= Values Section ======= -->
    <section id="values" class="values">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <h2>skelta</h2>
                <p>Why Skelta</p>
            </header>

            <div class="row">

                <div class="col-lg-4" data-aos="fade-up" data-aos-delay="200">
                    <div class="box">
                        <img src="{{asset('img/time.gif') }}" class="img-fluid" alt="">
                        <h3>Save Time & Money</h3>
                        <p>
                            Get variety of rates to compare at once, side by side. Select the policy that Suits your
                            budget
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="400">
                    <div class="box">
                        <img src="{{asset('img/biased.gif') }}" class="img-fluid" alt="">
                        <h3>Unbiased & Impartial</h3>
                        <p>Get Quotes from a range of Kenyan Trusted Insurance Company Brands</p>
                    </div>
                </div>

                <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="600">
                    <div class="box">
                        <img src="{{asset('img/quick.gif') }}" class="img-fluid" alt="">
                        <h3>Quick</h3>
                        <p>Instant use of cutting edge technology</p>
                    </div>
                </div>

            </div>

        </div>

    </section><!-- End Values Section -->


    <!-- PARTNER-->

    <section id="values" class="values">
        <div class="container">
            <header class="section-header">
                <h2>partners</h2>
                <p>Skelta Trusted Partners</p>
            </header>

            <div id="CarouselSliderPartner" class="carousel carousel-dark slide" data-bs-ride="carousel"
                 data-bs-interval="true">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col"><img src="{{asset('img/partner/23.png') }}" alt="1 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/2.png') }}" alt="2 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/13.png') }}" alt="3 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/4.png') }}" alt="4 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/5.png') }}" alt="5 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/6.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/13.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/14.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/15.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/16.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/17.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                        </div>
                        <div class="row">
                            <div class="col"><img src="{{asset('img/partner/22.png') }}" alt="4 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/8.png') }}" alt="5 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/9.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/25.png') }}" alt="4 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/11.png') }}" alt="5 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/24.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/17.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/34.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/19.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/20.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                            <div class="col"><img src="{{asset('img/partner/21.png') }}" alt="6 slide" width="80"
                                                  height="60"></div>
                        </div>
                    </div>

                </div>


                <button class="carousel-control-prev" type="button" data-bs-target="#CarouselSliderPartner"
                        data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true" hidden></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#CarouselSliderPartner"
                        data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true" hidden></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>
    </section>
    <!-- END PARTNER-->



@endsection

