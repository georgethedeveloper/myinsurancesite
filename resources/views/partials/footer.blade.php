<!-- ======= Footer ======= -->
<footer id="footer" class="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row gy-4">
                <div class="col-lg-5 col-md-12 footer-info">
                    <a href="/" class="logo d-flex align-items-center">
                        <img src="{{asset('img/icons/favicon-16x16.png') }}" alt="">
                        <span>Skelta</span>
                    </a>
                    <p>
                        Skelta is one of Kenya’s leading insurance comparison websites, has an exciting
                        opportunity for your company to grow your business.
                    </p>
                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="fab fa-twitter"></i>
                            <a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
                            <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
                            <a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
                    </div>
                </div>

                <div class="col-lg-2 col-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">Home</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">About us</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="compare-insurance">Compare Insurance</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-2 col-6 footer-links">
                    <h4>Our Services</h4>
                    <ul>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">Motor Insurance</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">Medical Insurance</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">Life Insurance</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">Personal Accident Insurance</a></li>
                        <li><i class="bi bi-chevron-right"></i> <a href="#">Travel Insurance</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
                    <h4>Contact Us</h4>
                    <p>
                        A108 Adam Street <br>
                        Nairobi, Nrb 535022<br>
                        Kenya <br><br>
                        <strong>Phone:</strong> +254 5589 55488 55<br>
                        <strong>Email:</strong> skelta@site.com<br>
                    </p>

                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            &copy; Copyright <strong><span>Skelta</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="https://georgethedeveloper.tech/">georgethedeveloper</a>
        </div>
    </div>
</footer><!-- End Footer -->
