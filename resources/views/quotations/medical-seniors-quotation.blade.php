@extends("layouts.master")

@section('title')
    Private Motor Quotation
@endsection

@section('content')

    <div class="page-content get-a-quote get-a-quote-3">
        <!-- HEADING PAGE-->
        <div class="heading-page heading-normal heading-project">
            <div class="container">
                <ul class="au-breadcrumb">
                    <li class="au-breadcrumb-item">
                        <i class="fa fa-home"></i>
                        <a href="/home">Home</a>
                    </li>
                    <li class="au-breadcrumb-item">
                        <i aria-hidden="true" class="fa fa-university"></i>
                        <a href="/compare-insurance">Compare Insurance</a>
                    </li>
                    <li class="au-breadcrumb-item ">
                        <i class="fa fa-heartbeat"></i>
                        <a href="/medical-insurance">Medical Insurance</a>
                    </li>
                    <li class="au-breadcrumb-item active">
                        <i class="fas fa-walking"></i>
                        <a href="/medical-seniors-quotation">Medical Senior Quotation</a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END HEADING PAGE-->
        <!-- Success message -->

        <div class="row">


        </div>

        <div class="quote-container">

            <div class="container">

                @if($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role='alert'>
                        <h4>{{$errors->first()}}</h4>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <!-- GET A QUOTE, STYLE 3-->
                        <section class="quote-form quote-form-style-3">
                            <div class="head-section">
                                <h4>Medical Senior Quotation</h4>
                            </div>

                            <form method="post" action="{{ route('medical-seniors-quotations-results') }}"
                                  class="row g-3 needs-validation" novalidate>
                                @csrf

                                <div class="quote-form-container">
                                    <div class="quote-form-heading">
                                        <h4>Start your Senior Medical Quote below.</h4>
                                    </div>

                                    <div class="col-md-12 position-relative">
                                        <label for="validationTooltip04" class="form-label text-white fw-bold">
                                            Limits <span class="text-danger"> *</span>
                                        </label>
                                        <select class="form-select dropdown" id="IPLimit" name="IPLimit" required>
                                            <option selected disabled value="">Choose a Limit</option>
                                            @if($medicalSeniorResponseBody)
                                                @foreach ($medicalSeniorResponseBody as $medicalSeniorResponse)
                                                    <option
                                                        value="{{ $medicalSeniorResponse}}">{{ $medicalSeniorResponse }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="invalid-tooltip">
                                            Please select a Limit.
                                        </div>
                                    </div>

                                    <div class="col-md-12 position-relative">
                                        <label for="validationTooltip03" class="form-label text-white fw-bold">
                                            Principal Age <span class="text-danger"> *</span>
                                        </label>
                                        <input type="number" class="form-control" id="principalAge" name="principalAge"
                                               required>
                                        <div class="invalid-tooltip">
                                            Please provide a Principal Age.
                                        </div>
                                    </div>

                                    <div class="col-md-12 position-relative">
                                        <label for="validationTooltip03" class="form-label text-white fw-bold">
                                            Spouse Age <span class="text-danger"> *</span>
                                        </label>
                                        <input type="number" class="form-control" id="spouseAge" name="spouseAge"
                                               required>
                                        <div class="invalid-tooltip">
                                            Please provide a Spouse Age.
                                        </div>
                                    </div>

                                    <div class="col-md-12 position-relative">
                                        <label for="validationTooltip03" class="form-label text-white fw-bold">
                                            Number Of Children <span class="text-danger"> *</span>
                                        </label>
                                        <input type="number" class="form-control" id="noOfChildren" name="noOfChildren"
                                               required>
                                        <div class="invalid-tooltip">
                                            Please provide a valid Number Of Children.
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <label for="validationTooltip04" class="form-label text-white fw-bold">
                                                OutPatient <span class="text-danger"> *</span>
                                            </label>
                                            <input type="number" class="form-control" id="OLimit" name="OLimit"
                                                   value=""
                                                   required>
                                            <div class="invalid-tooltip">
                                                Please provide a valid outPatient.
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="validationTooltip05" class="form-label text-white fw-bold">
                                                Dental <span class="text-danger"> *</span>
                                            </label>
                                            <input type="number" class="form-control" id="DLimit" name="DLimit"
                                                   value=""
                                                   required>
                                            <div class="invalid-tooltip">
                                                Please provide a valid dental.
                                            </div>
                                        </div>
                                        <div class="col">
                                            <label for="validationTooltip06" class="form-label text-white fw-bold">
                                                Optical <span class="text-danger"> *</span>
                                            </label>
                                            <input type="number" class="form-control" id="OPLimit" name="OPLimit"
                                                   value=""
                                                   required>
                                            <div class="invalid-tooltip">
                                                Please provide a valid optical.
                                            </div>
                                        </div>
                                    </div>

                                    <div class="privateSubmit">
                                        <button class="btn-warning btn" type="submit">Get a Quote</button>
                                    </div>
                                </div>

                                {{ method_field('POST') }}


                            </form>

                            <script>
                                // Example starter JavaScript for disabling form submissions if there are invalid fields
                                (function () {
                                    'use strict';

                                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                    var forms = document.querySelectorAll('.needs-validation');

                                    // Loop over them and prevent submission
                                    Array.prototype.slice.call(forms)
                                        .forEach(function (form) {
                                            form.addEventListener('submit', function (event) {
                                                if (!form.checkValidity()) {
                                                    event.preventDefault();
                                                    event.stopPropagation()
                                                }

                                                form.classList.add('was-validated')
                                            }, false)
                                        })
                                })()
                            </script>
                        </section>
                        <!-- END GET A QUOTE, STYLE 1-->
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
