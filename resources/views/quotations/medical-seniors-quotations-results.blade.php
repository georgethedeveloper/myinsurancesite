@extends("layouts.master")

@section('title')
    Motor Medical Senior Quotation
@endsection

@section('content')
    <!-- HEADING PAGE-->
    <div class="heading-page heading-normal heading-project">
        <div class="container">
            <ul class="au-breadcrumb">
                <li class="au-breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/home">Home</a>
                </li>
                <li class="au-breadcrumb-item">
                    <i class="fa fa-walking"></i>
                    <a href="/medical-seniors-quotation">Medical Senior Quotation</a>
                </li>
                <li class="au-breadcrumb-item active">
                    <i class="fas fa-poll"></i>
                    <a href="/medical-senior-quotations-results">Medical Senior Quotations Results</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END HEADING PAGE-->
    <!-- END HEADING PAGE-->
    <div class="page-content services-detail-1">
        <div class="container">
            <div class="post-paragraph p1">
                <div class="post-heading">
                    <h3>Medical Senior Quotation Results</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="post-with-image">
                        <div class="post-paragraph">
                            <div class="post-heading">
                                <h3>Limit</h3>
                            </div>
                            <div class="post-content">
                                <h4>KSH {{number_format($medicalSeniorResponseBody["inputs"]["IPLimit"]) }}</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="post-with-image">
                        <div class="post-paragraph">
                            <div class="post-heading">
                                <h3>Principal Age</h3>
                            </div>
                            <div class="post-content">
                                <h4>{{$medicalSeniorResponseBody["inputs"]["principalAge"] }} years</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="post-with-image">
                        <div class="post-paragraph">
                            <div class="post-heading">
                                <h3>Spouse Age</h3>
                            </div>
                            <div class="post-content">
                                <h4>{{$medicalSeniorResponseBody["inputs"]["spouseAge"] }}</h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                @foreach ($medicalSeniorResponseBody["resultsbyCover"]["INPATIENT"] as $medicalSeniorResponse)

                    <div class="col-md-4">
                        <div class="card mb-4">
                            <div class="row row-cols-1 row-cols-md-3 g-4">
                                <div class="col-md-4">
                                    <img class="img-fluid rounded-start"
                                         src="{{ asset('img/partner/'.$medicalSeniorResponse['CompanyID'].'png') }}"
                                         alt="Insurance Logo">
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body">
                                        <div
                                            class="card-header fw-bolder">{{$medicalSeniorResponse["ProductName"]}}</div>
                                        <p class="card-text"> Cover Type: <small
                                                class="text-muted fw-bold">{{$medicalSeniorResponse["Cover Type"]}}</small>
                                        </p>
                                        <p class="card-text"> Premium: <small
                                                class="card-title fw-bold">Ksh {{number_format($medicalSeniorResponse["premium"])}}</small>
                                        </p>
                                        <p class="card-text"> Principal Premium: <small
                                                class="text-muted fw-bold">Ksh {{number_format($medicalSeniorResponse["principal-premium"])}}</small>
                                        </p>
                                        <p class="card-text"> Spouse Premium: <small
                                                class="text-muted fw-bold">Ksh {{number_format($medicalSeniorResponse["spouse-premium"])}}</small>
                                        </p>
                                        <p class="card-text"> Child Premium: <small
                                                class="text-muted fw-bold">Ksh {{number_format($medicalSeniorResponse["child-premium"])}}</small>
                                        </p>
                                        <p class="card-text"> Limit Type: <small
                                                class="text-muted fw-bold">{{$medicalSeniorResponse["Limit Type"]}}</small>
                                        </p>
                                        <p>Covers:</p>
                                        @foreach ($medicalSeniorResponse["covers"] as $key=>$value)
                                            <ul class="list-inline">
                                                <li class="fw-bold" style="margin-right: -10px;">
                                                    <span class="bold" hidden> {{ $key }} </span> {{ $value }}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="post-content-box">

                         <div class="row">

                             <div class="col-sm-4">

                                 <div class="row">
                                     <div class="col-sm-6">
                                         <img class="card-img" style="padding: 15px 15px 15px 15px;"
                                              src="{{ asset('img/partner/'.$medicalSeniorResponse['id'].'png') }}"
                                              alt="Insurance Logo">
                                     </div>
                                     <div class="col-sm-6">
                                         <h4>{{$medicalSeniorResponse["key"]}}</h4>

                                         Premium:<p class="fw-bold">
                                             <strong>KSH {{number_format($medicalSeniorResponse["premium"],2)}}</strong>
                                         </p>

                                         Premium:<p class="fw-bold">
                                             <strong>{{($medicalSeniorResponse["ProductName"])}}</strong>
                                         </p>
                                     </div>
                                 </div>

                             </div>

                             <div class="col-sm-6">
                                 <div class="accordion accordion-flush"
                                      id='accordionExample-{{$medicalSeniorResponse["id"]}}'>

                                     <div class="accordion-item">
                                         <h2 class="accordion-header" id="headingOne-{{$medicalSeniorResponse['id']}}">
                                             <button class="accordion-button collapsed fw-bold active" type="button"
                                                     data-bs-toggle="collapse"
                                                     data-bs-target="#collapseOne-{{$medicalSeniorResponse['id']}}"
                                                     aria-expanded="false"
                                                     aria-controls="collapseOne-{{$medicalSeniorResponse['id']}}">
                                                 Additional Benefits
                                             </button>
                                         </h2>
                                         <div id="collapseOne-{{$medicalSeniorResponse['id']}}"
                                              class="accordion-collapse collapse show"
                                              aria-labelledby="headingOne-{{$medicalSeniorResponse['id']}}"
                                              data-bs-parent="#accordionExample-{{$medicalSeniorResponse['id']}}">
                                             <div class="accordion-body">
                                                 <ul>
                                                     @foreach ($medicalSeniorResponse["Additional-covers"] as $key=>$value)
                                                         <li><p><span class="bold"> {{ $key }} : </span> {{ $value }}
                                                             </p>
                                                         </li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="accordion-item">
                                         <h2 class="accordion-header" id="headingTwo-{{$medicalSeniorResponse['id']}}">
                                             <button class="accordion-button collapsed fw-bold" type="button"
                                                     data-bs-toggle="collapse"
                                                     data-bs-target="#collapseTwo-{{$medicalSeniorResponse['id']}}"
                                                     aria-expanded="false"
                                                     aria-controls="collapseTwo-{{$medicalSeniorResponse['id']}}">
                                                 Limits Of Liability
                                             </button>
                                         </h2>
                                         <div id="collapseTwo-{{$medicalSeniorResponse['id']}}"
                                              class="accordion-collapse collapse show"
                                              aria-labelledby="headingTwo-{{$medicalSeniorResponse['id']}}"
                                              data-bs-parent="#accordionExample-{{$medicalSeniorResponse['id']}}">
                                             <div class="accordion-body">
                                                 <ul>
                                                     @foreach ($medicalSeniorResponse["limits"] as $key=>$value)
                                                         <li><p><span class="bold"> {{ $key }} : </span> {{ $value }} </p>
                                                         </li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="accordion-item">
                                         <h2 class="accordion-header" id="headingThree-{{$medicalSeniorResponse['id']}}">
                                             <button class="accordion-button collapsed fw-bold" type="button"
                                                     data-bs-toggle="collapse"
                                                     data-bs-target="#collapseThree-{{$medicalSeniorResponse['id']}}"
                                                     aria-expanded="false"
                                                     aria-controls="collapseThree-{{$medicalSeniorResponse['id']}}">
                                                 Applicable Excesses
                                             </button>
                                         </h2>
                                         <div id="collapseThree-{{$medicalSeniorResponse['id']}}"
                                              class="accordion-collapse collapse"
                                              aria-labelledby="headingThree-{{$medicalSeniorResponse['id']}}"
                                              data-bs-parent="#accordionExample-{{$medicalSeniorResponse['id']}}">
                                             <div class="accordion-body">
                                                 <ul>
                                                     @foreach ($medicalSeniorResponse["excess"] as $key=>$value)
                                                         <li><p><span class="bold"> {{ $key }} : </span> {{ $value }} </p>
                                                         </li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                         </div>
                                     </div>


                                 </div>
                             </div>

                             <div class="col-sm-2">
                                 <a href="#" class="btn btn-success" style="margin-top:180%;">I am Interested</a>
                             </div>


                         </div>


                     </div>--}}

                @endforeach
            </div>


        </div>
    </div>


@endsection
