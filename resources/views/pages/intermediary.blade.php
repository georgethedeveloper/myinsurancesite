@extends("layouts.master")

@section("title")
    Insurance Details
@endsection

@section("content")
    <!-- HEADING PAGE-->
    <div class="heading-page heading-normal heading-project">
        <div class="container">
            <ul class="au-breadcrumb">
                <li class="au-breadcrumb-item">
                    <i class="fa fa-home"></i>
                    <a href="/home">Home</a>
                </li>
                <li class="au-breadcrumb-item active">
                    <i class="fas fa-globe-africa"></i>
                    <a href="intermediary">Intermediary World</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END HEADING PAGE-->


    <!-- ======= Features Section ======= -->
    <section id="features" class="features">

        <div class="container" data-aos="fade-up">

            <header class="section-header">
                <p>Welcome to our Intermediary World</p>
            </header>
            <!-- Feature Tabs -->
            <div class="row feture-tabs" data-aos="fade-up">
                <div class="col-lg-6">
                    <!-- Tabs -->
                    <ul class="nav nav-pills mb-3">
                        <li>
                            <a class="nav-link active" data-bs-toggle="pill" href="#tab1">Intermediary</a>
                        </li>
                        <li>
                            <a class="nav-link" data-bs-toggle="pill" href="#tab2">Join US</a>
                        </li>
                    </ul><!-- End Tabs -->

                    <!-- Tab Content -->
                    <div class="tab-content">

                        <div class="tab-pane fade show active" id="tab1">
                            <p>Skelta is one of Kenya’s leading insurance comparison websites, has an exciting
                                opportunity for your company to grow your business.</p>
                            <div class="d-flex align-items-center mb-2">
                                <i class="bi bi-check2"></i>
                                <h4>The Portal</h4>
                            </div>
                            <p>Our secure Intermediary portal helps you manage your product portfolio and client
                                accounts, obtain customized quotes and view recent activity, while we keep you informed
                                on the recent industry activities.</p>
                        </div><!-- End Tab 1 Content -->

                        <div class="tab-pane fade show" id="tab2">
                            <form class="row g-3">
                                <div class="col-md-12">
                                    <label for="validationDefault01" class="form-label">First name</label>
                                    <input type="text" class="form-control" id="validationDefault01" value="" required>
                                </div>
                                <div class="col-md-12">
                                    <label for="validationDefault02" class="form-label">Last name</label>
                                    <input type="text" class="form-control" id="validationDefault02" value="" required>
                                </div>
                                <div class="col-md-12">
                                    <label for="validationDefault03" class="form-label">Email</label>
                                    <input type="email" class="form-control" id="validationDefault03" required>
                                </div>

                                <div class="col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2"
                                               required>
                                        <label class="form-check-label" for="invalidCheck2">
                                            Agree to terms and conditions
                                        </label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-primary" type="submit">Join Us</button>
                                </div>
                            </form>
                        </div><!-- End Tab 2 Content -->


                    </div>

                </div>

                <div class="col-lg-6">
                    <img src="{{asset('img/world.gif') }}" class="img-fluid" alt="">
                </div>

            </div><!-- End Feature Tabs -->

            {{--benefits--}}
            <header class="section-header">
                <p>Benefits as an Intermediary or Agent</p>
            </header>

            <div class="row">
                <div class="col-lg-6">
                    <img src="{{asset('img/agent.gif') }}" class="img-fluid" alt="">
                </div>

                <div class="col-lg-6 mt-5 mt-lg-0 d-flex">
                    <div class="row align-self-center gy-4">

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>A personalized dashboard and an overview of your products</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="300">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Get comparison quotes for your customers complete with your logo</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="400">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Get details of benefits and negotiated rates as per your desired products</h3>
                            </div>
                        </div>

                        <div class="col-md-6" data-aos="zoom-out" data-aos-delay="500">
                            <div class="feature-box d-flex align-items-center">
                                <i class="bi bi-check"></i>
                                <h3>Request a personalized website with a powerful SEO</h3>
                            </div>
                        </div>


                    </div>
                </div>

            </div> <!-- / row -->
        {{--end of benefits--}}

        <!-- Feature Icons -->
            <div class="row feature-icons" data-aos="fade-up">
                <h3>The Benefits</h3>

                <div class="row">

                    <div class="col-xl-4 text-center" data-aos="fade-right" data-aos-delay="100">
                        <img src="{{asset('img/benefit.gif') }}" class="img-fluid p-4" alt="">
                    </div>

                    <div class="col-xl-8 d-flex content">
                        <div class="row align-self-center gy-4">

                            <div class="col-md-6 icon-box" data-aos="fade-up">
                                <i class="ri-line-chart-line"></i>
                                <div>
                                    <h4>No joining cost</h4>
                                    <p>There is no cost to have your quote on our website, you can always share the
                                        quotations with the client on email or any social media, and it is FREE.</p>
                                </div>
                            </div>

                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                                <i class="ri-stack-line"></i>
                                <div>
                                    <h4>Great exposure</h4>
                                    <p>Your brand will benefit from the awareness of being listed in our quote results
                                        and enjoy the exposure we create with our extensive advertising campaigns. Boost
                                        your brand image, awareness, recognition and reputation.</p>
                                </div>
                            </div>

                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                                <i class="ri-brush-4-line"></i>
                                <div>
                                    <h4>We are growing fast</h4>
                                    <p>Skelta site is growing fast in popularity in Kenya and across Africa. As more
                                        customers shop around for a great deal and as Internet usage increases in
                                        Africa, so will your business grow!</p>
                                </div>
                            </div>

                            <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                                <i class="ri-magic-line"></i>
                                <div>
                                    <h4>Impartial and secure</h4>
                                    <p>Skelta limited operates as an independent brand. The quotation results that are
                                        shown are in not influenced by any other factors other than the order of the
                                        best-quoted price at any particular moment. Your data is handled securely using
                                        the best Internet security and safe data handling measures. Quick and easy to
                                        get you up and running, let partner now</p>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>

            </div><!-- End Feature Icons -->

        </div>

    </section><!-- End Features Section -->

@endsection
