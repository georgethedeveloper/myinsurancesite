<?php
/*
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MotorCommercialController extends Controller
{
    //
}*/


namespace App\Http\Controllers;
#import config

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;
use Illuminate\Http\Request;
use Redirect;

#use http\Client\Request;

class MotorCommercialController extends Controller
{
    public function getCommercialCars()
    {
        try {
            /*$url = config('services.appServer.app_token');*/
            $client = new Client(); //GuzzleHttp\Client
            $url = "http://188.166.123.24:5001/get_cars";

            /*$commercialResponse = Http::get('http://188.166.123.24:5001/get_cars');*/


            #$url= config["appServer"]."/get-cars"

            $commercialResponse = $client->request('GET', $url, [
                'verify' => false,
            ]);


            $commercialResponseBody = json_decode($commercialResponse->getBody());
            /*dd('$commercialResponseBod', $commercialResponseBody);*/
        } catch (ClientException $e) {
            echo Psr7\Message::toString($e->getRequest());
            echo Psr7\Message::toString($e->getResponse());
        } catch (Exception $exception) {
            $commercialResponseBody = [];
            return view('quotations.motor-commercial-quotation', compact('commercialResponseBody'))->withErrors(['msg' => 'Error Processing Quotation . Please try Again']);
        }

        return view('quotations.motor-commercial-quotation', compact('commercialResponseBody'));
    }

    public function motorCommercialSaveQuote(Request $request)
    {
        try {
            $client = new Client(['http_errors' => false]); //GuzzleHttp\Client
            $url = "http://188.166.123.24:5001/motorcomm-save-quote";


            $motorCommercialArray = array(
                "vehicleValue" => $request->vehicleValue,
                "yom" => $request->yom,
                "class" => $request->class,
                "vehicleAge" => $request->vehicleAge,
                "seats" => $request->seats,
                "dt" => $request->dt,
                "result size" => $request->resultSize

                /*class
                1=
                2=
                3=
                4=Own Goods*/


                /*"vehicleID" => $request->make,*/
            );
            //echo "\r\n motorCommercialArray ".json_encode($motorCommercialArray);

            $commercialResponse = $client->post($url, [
                'json' => $motorCommercialArray,
            ]);

            #$content=(string)$commercialResponse->getBody()->__toString();

            $content = (string)$commercialResponse->getBody()->getContents();
            $content = trim($content);
            #$contents2=$commercialResponse->json();
            #dd($content);


            #$content= trim($content);
            $commercialResponseBody = json_decode($content, true);

        } catch (ClientException $e) {
            echo Psr7\Message::toString($e->getRequest());
            echo Psr7\Message::toString($e->getResponse());
        } catch (Exception $exception) {
            #dd()
            return Redirect::back()->withErrors(['msg' => 'Error processing  Quotations . Please try Again']);
        }
        #echo "<br>"."\r\n Make - " .$request->make;
        #echo "<br>"."\r\n Value - " .$request->vehicleValue;
        #echo "<br>"."\r\n yom - " .$request->yom;

        #echo "<br>json lst error -".json_last_error();

        #dd($content);
        #dd($commercialResponseBody );

        return view('quotations.motor-commercial-quotations-results', compact('commercialResponseBody'));
    }


}
