<?php

namespace App\Http\Controllers;
#import config

use Exception;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;
use Illuminate\Http\Request;
use Redirect;

#use http\Client\Request;

class MedicalSeniorController extends Controller
{
    public function getIPLimits()
    {
        try {
            /*$url = config('services.appServer.app_token');*/
            $client = new Client(); //GuzzleHttp\Client
            $url = "http://188.166.123.24:5001/get_IPLimits";

            /*$medicalSeniorResponse = Http::get('http://188.166.123.24:5001/get_cars');*/


            #$url= config["appServer"]."/get-cars"

            $medicalSeniorResponse = $client->request('GET', $url, [
                'verify' => false,
            ]);


            $medicalSeniorResponseBody = json_decode($medicalSeniorResponse->getBody());
            /*dd('$medicalSeniorResponseBod', $medicalSeniorResponseBody);*/
        } catch (ClientException $e) {
            echo Psr7\Message::toString($e->getRequest());
            echo Psr7\Message::toString($e->getResponse());
        } catch (Exception $exception) {
            $medicalSeniorResponseBody = [];
            return view('quotations.medical-seniors-quotation', compact('medicalSeniorResponseBody'))->withErrors(['msg' => 'Server Error. Please try Again']);
        }

        return view('quotations.medical-seniors-quotation', compact('medicalSeniorResponseBody'));
    }

    public function medSeniorsSaveQuote(Request $request)
    {
        try {
            $client = new Client(['http_errors' => false]); //GuzzleHttp\Client
            $url = "http://188.166.123.24:5001/medSeniors-save-quote";


            $medicalSeniorArray = array(
                "IPLimit" => $request->IPLimit,
                "principalAge" => $request->principalAge,
                "spouseAge" => $request->spouseAge,
                "noOfChildren" => $request->noOfChildren,
                "OPLimit" => $request->OPLimit,
                "DLimit" => $request->DLimit,
                "OLimit" => $request->OLimit,
            );
            #echo "\r\n medicalSeniorArray ".json_encode($medicalSeniorArray);

            $medicalSeniorResponse = $client->post($url, [
                'json' => $medicalSeniorArray,
            ]);

            #$content=(string)$medicalSeniorResponse->getBody()->__toString();

            $content = (string)$medicalSeniorResponse->getBody()->getContents();
            $content = trim($content);
            #$contents2=$medicalSeniorResponse->json();
            #dd($content);


            $content = trim($content);
            $medicalSeniorResponseBody = json_decode($content, true);

        } catch (ClientException $e) {
            echo Psr7\Message::toString($e->getRequest());
            echo Psr7\Message::toString($e->getResponse());
        } catch (Exception $exception) {
            #dd()
            return Redirect::back()->withErrors(['msg' => 'Error processing  Quotation . Please try Again']);
        }
        #echo "<br>"."\r\n IPLimit - " .$request->IPLimit;
        #echo "<br>"."\r\n principalAge - " .$request->principalAge;
        #echo "<br>"."\r\n spouseAge - " .$request->spouseAge;

        #echo "<br>json lst error -".json_last_error();

        #dd($content);
        #dd($medicalSeniorResponseBody );

        return view('quotations.medical-seniors-quotations-results', compact('medicalSeniorResponseBody'));
    }


}
