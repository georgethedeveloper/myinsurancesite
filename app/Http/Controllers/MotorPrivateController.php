<?php

namespace App\Http\Controllers;
#import config

use Exception;
use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;
use Illuminate\Http\Request;
use Redirect;

#use http\Client\Request;

class MotorPrivateController extends Controller
{
    public function getPrivateCars()
    {
        try {
            /*$url = config('services.appServer.app_token');*/
            $client = new Client(); //GuzzleHttp\Client
            $url = "http://188.166.123.24:5001/get_cars";

            /*$privateResponse = Http::get('http://188.166.123.24:5001/get_cars');*/


            #$url= config["appServer"]."/get-cars"

            $privateResponse = $client->request('GET', $url, [
                'verify' => false,
            ]);


            $privateResponseBody = json_decode($privateResponse->getBody());
            /*dd('$privateResponseBod', $privateResponseBody);*/
        } catch (ClientException $e) {
            echo Psr7\Message::toString($e->getRequest());
            echo Psr7\Message::toString($e->getResponse());
        } catch (Exception $exception) {
            $privateResponseBody = [];
            return view('quotations.motor-private-quotation', compact('privateResponseBody'))->withErrors(['msg' => 'Server Error. Please try Again']);
        }

        return view('quotations.motor-private-quotation', compact('privateResponseBody'));
    }

    public function motorSaveQuote(Request $request)
    {
        try {
            $client = new Client(['http_errors' => false]); //GuzzleHttp\Client
            $url = "http://188.166.123.24:5001/motor-save-quote2";


            $motorPrivateArray = array(
                "vehicleValue" => $request->vehicleValue,
                "vehicleID" => $request->make,
                "yom" => $request->yom,
                "dt" => $request->dt,
                "result size" => $request->resultSize
            );
            //echo "\r\n motorPrivateArray ".json_encode($motorPrivateArray);

            $privateResponse = $client->post($url, [
                'json' => $motorPrivateArray,
            ]);

            #$content=(string)$privateResponse->getBody()->__toString();

            $content = (string)$privateResponse->getBody()->getContents();
            $content = trim($content);
            #$contents2=$privateResponse->json();
            #dd($content);


            #$content= trim($content);
            $privateResponseBody = json_decode($content, true);

        } catch (ClientException $e) {
            echo Psr7\Message::toString($e->getRequest());
            echo Psr7\Message::toString($e->getResponse());
        } catch (Exception $exception) {
            #dd()
            return Redirect::back()->withErrors(['msg' => 'Error processing  Quotation . Please try Again']);
        }
        #echo "<br>"."\r\n Make - " .$request->make;
        #echo "<br>"."\r\n Value - " .$request->vehicleValue;
        #echo "<br>"."\r\n yom - " .$request->yom;

        #echo "<br>json lst error -".json_last_error();

        #dd($content);
        #dd($privateResponseBody );

        return view('quotations.motor-private-quotations-results', compact('privateResponseBody'));
    }


}
