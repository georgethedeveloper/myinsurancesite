<?php


use App\Http\Controllers\MedicalSeniorController;
use App\Http\Controllers\MotorCommercialController;
use App\Http\Controllers\MotorPrivateController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('about', function () {
    return view('pages/about');
});


Route::get('/agent-list', function () {
    return view('pages/agent-list');
});

Route::get('/articles', function () {
    return view('pages/articles');
});

Route::get('/article-detail', function () {
    return view('pages/article-detail');
});

/*******************Compare Insurance Pages***********************************/

Route::get('/compare-insurance', function () {
    return view('quotations/compare-insurance');
});

/********************************Quotation Pages**************************************************/

/*****************Private Motor*/
Route::get('motor-private-quotation', [MotorPrivateController::class, 'getPrivateCars'])->name('motor-private-quotation');

Route::post('motor-private-quotations-results', [MotorPrivateController::class, 'motorSaveQuote'])->name('motor-private-quotations-results');
/*****************Private Motor*/

/*****************Motor Commercial*/
Route::get('/motor-commercial', function () {
    return view('quotations/motor-commercial');
});

Route::get('motor-commercial-quotation', [MotorCommercialController::class, 'getCommercialCars'])->name('motor-commercial-quotation');

Route::post('motor-commercial-quotations-results', [MotorCommercialController::class, 'motorCommercialSaveQuote'])->name('motor-commercial-quotations-results');

/*****************Motor Commercial*/

/************Medical Insurance */
Route::get('/medical-insurance', function () {
    return view('quotations/medical-insurance');
});

Route::get('medical-seniors-quotation', [MedicalSeniorController::class, 'getIPLimits'])->name('medical-seniors-quotation');

Route::post('medical-seniors-quotations-results', [MedicalSeniorController::class, 'medSeniorsSaveQuote'])->name('medical-seniors-quotations-results');


/*******medical Insurance*/

Route::get('/contact', function () {
    return view('pages/contact');
});

Route::get('/intermediary', function () {
    return view('pages/intermediary');
});


Route::get('/insurance-products', function () {
    return view('pages/insurance-products');
});

Route::get('/insurance-details', function () {
    return view('pages/insurance-details');
});

Route::get('/insurance-product-detail', function () {
    return view('pages/insurance-product-detail');
});

Route::get('/underconstruction', function () {
    return view('pages/underconstruction');
});

Route::get('/404', function () {
    return view('pages/error-page');
});

/*Route::resource('quotations', MotorPrivateController::class);*/

/*Route::get('/getaquote', function () {
    return view('quotations/getaquote');
});

Route::get('/quotationsresults', function () {
    return view('quotations/quotationsresults');
});*/
